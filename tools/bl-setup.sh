#!/bin/bash

# Setting up the build envronment for the GPIB v2.x Arduino bootloader
# Operaing system: Ubuntu 20.04 LTS - WSL2 version

apt -y install make avr-libc gcc-avr binutils-avr

mkdir addon
cd addon
git clone https://github.com/abcminiuser/lufa.git LUFA
cd LUFA
git fetch --all --tags
git checkout tags/LUFA-111009
cd ../..

mkdir src
cd src
git clone https://gitlab.com/measurementcontrol/gpibinterfacefirmware.git
cd gpibinterfacefirmware/bootloader
make all



